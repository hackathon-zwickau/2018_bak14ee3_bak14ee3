// Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See LICENSE.txt in the project root for license information.
let express = require('express');
let router = express.Router();
let authHelper = require('../helpers/auth');
let graph = require('@microsoft/microsoft-graph-client');

/* GET /mail */
router.get('/', async function (req, res, next) {
    let parms = {title: 'Survey', active: {survey: true}};

    const accessToken = await authHelper.getAccessToken(req.cookies, res);
    const userName = req.cookies.graph_user_name;

    if (accessToken && userName) {
        parms.user = userName;

        // Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });

        try {

            const c_result = await client.api('/me/contacts').get();
            parms.contacts = [];
            let i = 0;
            c_result.value.forEach(function (obj) {
                parms.contacts.push(
                    {
                        "id": i++,
                        "displayName": obj.displayName,
                        "email": obj.emailAddresses[0].address
                    }
                )
            });

            // console.log((parms.contacts));

            parms.e_types = ["-", "Study", "Private"];
            parms.s_as = ["-", "Free", "Working elsewhere", "Tentative", "Busy", "Away"];
            parms.reminder = [
                "-",
                "5 min",
                "15 min",
                "20 min",
                "25 min",
                "30 min",
                "35 min",
                "40 min",
                "45 min",
                "50 min",
                "55 min",
                "60 min"
            ];

            res.render('meeting', parms);
        } catch (err) {
            parms.message = 'Error retrieving messages';
            parms.error = {status: `${err.code}: ${err.message}`};
            parms.debug = JSON.stringify(err.body, null, 2);
            res.render('error', parms);
        }

    } else {
        // Redirect to home
        res.redirect('/');
    }
});

/* POST /mail */
router.post('/', async function (req, res, next) {
        let parms = {title: 'Calendar', active: {calendar: true}};

        const accessToken = await authHelper.getAccessToken(req.cookies, res);
        const userName = req.cookies.graph_user_name;

        if (accessToken && userName) {
            parms.user = userName;

            let attendeesList = [];
            req.body.user.forEach(function (nn) {
                let splt = nn.split("#");
                attendeesList.push({
                    emailAddress: {
                        address: splt[1],
                        name: splt[0]
                    },
                    type: "Required"
                });
            });

            const client = graph.Client.init({
                authProvider: (done) => {
                    done(null, accessToken);
                }
            });
            let new_email = req.body.email;
            let new_location = req.body.location;
            let new_etitle = req.body.e_title;
            let new_etype = req.body.e_type;

            let new_start = req.body.d_start;
            let new_end = req.body.d_end;

            let new_reminder = req.body.d_reminder;
            let news_as = req.body.s_as;
            let new_gname = req.body.g_name;

            let new_event = {
                subject: new_etitle,
                body: {
                    contentType: "HTML",
                    content: new_etype + "! " + new_etitle
                },
                start: {
                    dateTime: new_start,
                    timeZone: "UTC"
                },
                end: {
                    "DateTime": new_end,
                    "TimeZone": "UTC"
                },
                location: {
                    displayName: new_location
                },
                attendees: attendeesList
            };

            // Set start of the calendar view to today at midnight
            const start = new Date(new Date().setHours(0, 0, 0));
            // Set end of the calendar view to 7 days from start
            const end = new Date(new Date(start).setDate(start.getDate() + 7));

            try {

                const resultee = await client.api('/me/events').create(new_event);


                const result = await client
                    .api(`/me/calendarView?startDateTime=${start.toISOString()}&endDateTime=${end.toISOString()}`)
                    .top(10)
                    .select('subject,start,end,attendees,id')
                    .orderby('start/dateTime DESC')
                    .get();

                parms.events = result.value;

                res.render('calendar', parms);
            } catch (err) {
                parms.message = 'Error retrieving events';
                parms.error = {status: `${err.code}: ${err.message}`};
                parms.debug = JSON.stringify(err.body, null, 2);
                res.render('error', parms);
            }

        }
        else {
            // Redirect to home
            res.redirect('/');
        }
    }
);

module.exports = router;