// Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See LICENSE.txt in the project root for license information.
let express = require('express');
let router = express.Router();
let authHelper = require('../helpers/auth');
let graph = require('@microsoft/microsoft-graph-client');

/* GET home page. */
router.get('/', async function (req, res, next) {
    let parms = {title: 'Home', active: {home: true}};

    const accessToken = await authHelper.getAccessToken(req.cookies, res);
    const userName = req.cookies.graph_user_name;

    if (accessToken && userName) {
        parms.user = userName;
        parms.debug = `User: ${userName}\nAccess Token: ${accessToken}`;

        // Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });

        const c_result = await client.api('/me/contacts').get();
        parms.contacts = c_result.value.length;

        // Set start of the calendar view to today at midnight
        const start = new Date(new Date().setHours(0, 0, 0));
        // Set end of the calendar view to 7 days from start
        const end = new Date(new Date(start).setDate(start.getDate() + 7));

        const e_result = await client
            .api(`/me/calendarView?startDateTime=${start.toISOString()}&endDateTime=${end.toISOString()}`)
            .get();
        parms.events = e_result.value.length;

    } else {
        parms.signInUrl = authHelper.getAuthUrl();
        parms.debug = parms.signInUrl;
    }

    res.render('index', parms);
});

module.exports = router;
