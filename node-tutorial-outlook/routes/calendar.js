// Copyright (c) Microsoft. All rights reserved. Licensed under the MIT license. See LICENSE.txt in the project root for license information.
let express = require('express');
let router = express.Router();
let authHelper = require('../helpers/auth');
let graph = require('@microsoft/microsoft-graph-client');

function getAttendeesStrings(attendees) {
    let displayStrings = {
        required: '',
        optional: '',
        resources: ''
    };

    attendees.forEach(function (attendee) {
        let attendeeName = (attendee.emailAddress.name === undefined) ?
            attendee.emailAddress.address : attendee.emailAddress.name;
        switch (attendee.type) {
            // Required
            case "required":
                if (displayStrings.required.length > 0) {
                    displayStrings.required += '; ' + attendeeName;
                }
                else {
                    displayStrings.required += attendeeName;
                }
                break;
            // Optional
            case "optional":
                if (displayStrings.optional.length > 0) {
                    displayStrings.optional += '; ' + attendeeName;
                }
                else {
                    displayStrings.optional += attendeeName;
                }
                break;
            // Resources
            case "resource":
                if (displayStrings.resources.length > 0) {
                    displayStrings.resources += '; ' + attendeeName;
                }
                else {
                    displayStrings.resources += attendeeName;
                }
                break;
        }
    });

    return displayStrings;
}

/* GET /calendar */
router.get('/', async function (req, res, next) {
    let parms = {title: 'Calendar', active: {calendar: true}};

    const accessToken = await authHelper.getAccessToken(req.cookies, res);
    const userName = req.cookies.graph_user_name;

    if (accessToken && userName) {
        parms.user = userName;

        // Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });

        // Set start of the calendar view to today at midnight
        const start = new Date(new Date().setHours(0, 0, 0));
        // Set end of the calendar view to 7 days from start
        const end = new Date(new Date(start).setDate(start.getDate() + 7));

        try {
            // Get the first 10 events for the coming week
            const result = await client
                .api(`/me/calendarView?startDateTime=${start.toISOString()}&endDateTime=${end.toISOString()}`)
                .top(10)
                .select('subject,start,end,attendees,id')
                .orderby('start/dateTime DESC')
                .get();

            parms.events = result.value;

            console.log("Events: \n " + (parms.events));

            res.render('calendar', parms);
        } catch (err) {
            parms.message = 'Error retrieving events';
            parms.error = {status: `${err.code}: ${err.message}`};
            parms.debug = JSON.stringify(err.body, null, 2);
            res.render('error', parms);
        }

    } else {
        // Redirect to home
        res.redirect('/');
    }
});

router.get('/updateItem', async function (req, res, next) {
    let parms = {title: 'Calendar', active: {calendar: true}};
    let itemId = req.query.eventId;

    const accessToken = await authHelper.getAccessToken(req.cookies, res);
    const userName = req.cookies.graph_user_name;
    if (accessToken && userName && itemId) {
        parms.user = userName;

        // Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });

        let newSubject = req.query.subject;
        let newLocation = req.query.location;
        let newReminderMins = parseInt(req.query.reminderMins);
        let newStartTime = req.query.startTime;
        let newEndTime = req.query.endTime;

        let updatePayload = {
            subject: newSubject,
            location: {
                displayName: newLocation
            },
            reminderMinutesBeforeStart: newReminderMins,
            end: {
                dateTime: newEndTime,
                timeZone: 'UTC'
            },
            start: {
                dateTime: newStartTime,
                timeZone: 'UTC'
            }
        };
        console.log("!!! " + JSON.stringify(updatePayload));
        try {
            const result = await client
                .api(`/me/events/${itemId}`)
                .patch(updatePayload);
            console.log(result);

            //parms.events = result;

            //console.log("Events: \n " + JSON.stringify(parms.events));
            res.redirect('/calendar');
        } catch (err) {
            parms.message = 'Error retrieving events';
            parms.error = {status: `${err.code}: ${err.message}`};
            parms.debug = JSON.stringify(err.body, null, 2);
            res.render('error', parms);
        }

    } else {
        // Redirect to home
        res.redirect('/');
    }
});

router.get('/deleteItem', async function (req, res, next) {
    let parms = {title: 'Calendar', active: {calendar: true}};
    let itemId = req.query.id;

    const accessToken = await authHelper.getAccessToken(req.cookies, res);
    const userName = req.cookies.graph_user_name;
    if (accessToken && userName && itemId) {
        parms.user = userName;

        // Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });

        try {
            const result = await client
                .api(`/me/events/${itemId}`)
                .delete();
            console.log(result);

            //parms.events = result;

            //console.log("Events: \n " + JSON.stringify(parms.events));
            res.redirect('/calendar');
        } catch (err) {
            parms.message = 'Error retrieving events';
            parms.error = {status: `${err.code}: ${err.message}`};
            parms.debug = JSON.stringify(err.body, null, 2);
            res.render('error', parms);
        }

    } else {
        // Redirect to home
        res.redirect('/');
    }
});

router.get('/viewItem', async function (req, res, next) {
    let parms = {title: 'Calendar', active: {calendar: true}};
    let itemId = req.query.id;

    const accessToken = await authHelper.getAccessToken(req.cookies, res);
    const userName = req.cookies.graph_user_name;

    if (accessToken && userName && itemId) {
        parms.user = userName;

        // Initialize Graph client
        const client = graph.Client.init({
            authProvider: (done) => {
                done(null, accessToken);
            }
        });
        try {
            // Get the first 10 events for the coming week
            const result = await client
                .api(`/me/events/${itemId}`)
                //.select('subject,attendees,location,start,end,isReminderOn,reminderMinutesBeforeStart')
                .get();

            parms.event = result;
            //console.log(result.attendees)
            parms.attendeeStrings = getAttendeesStrings(result.attendees);
            //console.log(parms.attendeeStrings)
            //console.log("item id: " + itemId);
            // console.log("Events: \n " + (parms));

            res.render('calendar_item', parms);
        } catch (err) {
            parms.message = 'Error retrieving events';
            parms.error = {status: `${err.code}: ${err.message}`};
            parms.debug = JSON.stringify(err.body, null, 2);
            res.render('error', parms);
        }

    } else {
        // Redirect to home
        res.redirect('/');
    }
});


module.exports = router;