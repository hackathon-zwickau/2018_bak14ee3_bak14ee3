let express = require('express');
let router = express.Router();
let request = require('request');
var ssn;

router.get("/", async function (req, res, next) {
    res.render("login")
});

router.post("/", async function (req, res, next) {

    ssn = req.session;
    let parms = {title: 'Home', active: {index: true}};

    let new_username = req.body.username;
    let new_password = req.body.password;

    let username = new_username,
        password = new_password,
        url = "http://localhost:9094",
        auth = "Basic " + new Buffer(username + ":" + password).toString("base64");
    ssn.authToken = auth;
    /*request(
        {
            url: url,
            headers: {
                "Authorization": auth
            }
        },
        function (error, response, body) {
            res.render('error', error);
        }
    );
*/

    res.render('index', parms);
});

module.exports = router;