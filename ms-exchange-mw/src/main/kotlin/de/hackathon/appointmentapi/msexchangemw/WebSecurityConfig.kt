package de.hackathon.appointmentapi.msexchangemw

import microsoft.exchange.webservices.data.core.ExchangeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter


@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    private val REALM = "MY_TEST_REALM"

    @Autowired
    lateinit var exchangeService: ExchangeService

    /*
        @Throws(Exception::class)
        protected override fun configure(http: HttpSecurity) {
            http.csrf().disable().authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers(HttpMethod.POST, "/login").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    // We filter the api/login requests
                    .addFilterBefore(JWTLoginFilter("/login", authenticationManager()),
                            UsernamePasswordAuthenticationFilter::class.java)
                    // And filter other requests to check the presence of JWT in header
                    .addFilterBefore(JWTAuthFilter(),
                            UsernamePasswordAuthenticationFilter::class.java)
        }
    */
    @Autowired
    @Throws(Exception::class)
    fun configureGlobalSecurity(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(CustomAuthProvider(exchangeService))
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/user/**").hasRole("ADMIN")
                .and().httpBasic().realmName(REALM).authenticationEntryPoint(getBasicAuthEntryPoint())

        http.addFilterAfter(CustomFilter(),
                BasicAuthenticationFilter::class.java)
    }

    @Bean
    fun getBasicAuthEntryPoint(): CustomBasicAuthenticationEntryPoint {
        return CustomBasicAuthenticationEntryPoint()
    }

    /* To allow Pre-flight [OPTIONS] request from browser */
    @Throws(Exception::class)
    override fun configure(web: WebSecurity?) {
        web!!.ignoring().antMatchers(HttpMethod.OPTIONS, "/**")
    }
}