package de.hackathon.appointmentapi.msexchangemw

data class AccountCredentials(val username: String, val password: String)