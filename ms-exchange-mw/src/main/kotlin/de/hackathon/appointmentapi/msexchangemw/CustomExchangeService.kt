package de.hackathon.appointmentapi.msexchangemw

import microsoft.exchange.webservices.data.EWSConstants
import microsoft.exchange.webservices.data.core.CookieProcessingTargetAuthenticationStrategy
import microsoft.exchange.webservices.data.core.EwsSSLProtocolSocketFactory
import microsoft.exchange.webservices.data.core.ExchangeService
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion
import org.apache.http.config.RegistryBuilder
import org.apache.http.conn.socket.ConnectionSocketFactory
import org.apache.http.conn.socket.PlainConnectionSocketFactory
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.impl.client.HttpClients
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager
import org.apache.http.ssl.SSLContextBuilder

class CustomExchangeService @Throws(Exception::class)
constructor(requestedServerVersion: ExchangeVersion) : ExchangeService(requestedServerVersion) {

    init {
        initializeHttpClient()
    }

    @Throws(Exception::class)
    private fun initializeHttpClient() {
        val registry = RegistryBuilder.create<ConnectionSocketFactory>()
                .register(EWSConstants.HTTP_SCHEME, PlainConnectionSocketFactory())
                .register(EWSConstants.HTTPS_SCHEME, EwsSSLProtocolSocketFactory.build(null, NoopHostnameVerifier.INSTANCE
                ))
                .build()

        val httpConnectionManager = PoolingHttpClientConnectionManager(registry)
        val authStrategy = CookieProcessingTargetAuthenticationStrategy()

        httpClient = HttpClients.custom()
                .setConnectionManager(httpConnectionManager)
                .setTargetAuthenticationStrategy(authStrategy)
                .setSSLContext(SSLContextBuilder().loadTrustMaterial(null) { x509Certificates, _ ->
                    for (certificate in x509Certificates) {
                        println("Check isTrusted for " + certificate.toString())
                    }
                    true
                }.build())
                .build()
    }
}