package de.hackathon.appointmentapi.msexchangemw.entity

data class MessageObj(val msgId: String, val msgTopic: String)