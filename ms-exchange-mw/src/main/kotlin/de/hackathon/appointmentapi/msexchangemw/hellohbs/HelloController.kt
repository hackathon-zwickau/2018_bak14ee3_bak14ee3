package de.hackathon.appointmentapi.msexchangemw.hellohbs;

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
@RequestMapping("/hello")
class HelloController {

    @GetMapping
    fun index(model: Model, principal: Principal): String {
        model.addAttribute("name", principal.name)
        return "hello"
    }

}