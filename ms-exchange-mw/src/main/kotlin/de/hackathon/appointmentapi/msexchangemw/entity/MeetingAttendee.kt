package de.hackathon.appointmentapi.msexchangemw.entity

data class MeetingAttendee(val isRequired: Boolean, val address: String, val name: String)