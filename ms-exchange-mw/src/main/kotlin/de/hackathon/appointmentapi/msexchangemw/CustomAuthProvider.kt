package de.hackathon.appointmentapi.msexchangemw

import microsoft.exchange.webservices.data.core.ExchangeService
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName
import microsoft.exchange.webservices.data.core.service.folder.Folder
import microsoft.exchange.webservices.data.credential.WebCredentials
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import java.net.URI

class CustomAuthProvider(val exchangeService: ExchangeService) : AuthenticationProvider {

    override fun supports(authentication: Class<*>?): Boolean {
        return true
    }

    override fun authenticate(authentication: Authentication?): Authentication? {
        if (authentication != null) {
            try {
                val credentials = WebCredentials(authentication.principal.toString(), authentication.credentials.toString())
                exchangeService.credentials = credentials
                exchangeService.url = URI("https://mail1.fh-zwickau.de/ews/exchange.asmx")
                exchangeService.validate()
                Folder.bind(exchangeService, WellKnownFolderName.Inbox)
                return authentication
            } catch (exc: Exception) {
                exc.printStackTrace()
            }
        }
        return null
    }
}