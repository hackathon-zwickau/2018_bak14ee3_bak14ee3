package de.hackathon.appointmentapi.msexchangemw

import de.hackathon.appointmentapi.msexchangemw.entity.*
import microsoft.exchange.webservices.data.core.ExchangeService
import microsoft.exchange.webservices.data.core.PropertySet
import microsoft.exchange.webservices.data.core.enumeration.property.BodyType
import microsoft.exchange.webservices.data.core.enumeration.property.EmailAddressKey
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName
import microsoft.exchange.webservices.data.core.enumeration.search.ResolveNameSearchLocation
import microsoft.exchange.webservices.data.core.service.folder.CalendarFolder
import microsoft.exchange.webservices.data.core.service.folder.Folder
import microsoft.exchange.webservices.data.core.service.item.Appointment
import microsoft.exchange.webservices.data.core.service.item.Contact
import microsoft.exchange.webservices.data.misc.NameResolution
import microsoft.exchange.webservices.data.property.complex.Attendee
import microsoft.exchange.webservices.data.property.complex.MessageBody
import microsoft.exchange.webservices.data.search.CalendarView
import microsoft.exchange.webservices.data.search.FolderView
import microsoft.exchange.webservices.data.search.ItemView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import java.security.Principal
import java.util.*
import java.util.function.Consumer


@RestController
@RequestMapping("/api")
class MwMsApi {

    @Autowired
    lateinit var exchangeService: ExchangeService

    @GetMapping("/10msgs")
    fun get10Msgs(principal: Principal): List<MessageObj> {
        val inbox = Folder.bind(exchangeService, WellKnownFolderName.Inbox)
        val view = ItemView(10)
        val findResults = exchangeService.findItems(inbox.id, view)

        //MOOOOOOST IMPORTANT: load messages' properties before
        exchangeService.loadPropertiesForItems(findResults, PropertySet.FirstClassProperties)
        val response = mutableListOf<MessageObj>()
        for (item in findResults.getItems()) {
            // Do something with the item as shown
            println("id==========" + item.getId())
            println("sub==========" + item.getSubject())
            response.add(MessageObj(item.id.toString(), item.subject))
        }
        return response
    }

    @GetMapping("/contacts")
    fun getAllContacts(principal: Principal): List<ContactData> {
        val response = mutableListOf<ContactData>()
        val folderFindResults = exchangeService.findFolders(WellKnownFolderName.Root, FolderView(Integer.MAX_VALUE))
        var kontakteVerzeichnis: Folder? = null

        // To keep the request smaller, request only the display name property.
//        var contactItems:FindItemsResults<Item> = exchangeService.findItems(WellKnownFolderName.Contacts, null);
        for (folder in folderFindResults.folders) {
            println("Count======" + folder.childFolderCount)
            println("Name=======" + folder.displayName)
            if (folder.displayName == "Alle Kontakte" || folder.displayName == "AllContacts") {
                kontakteVerzeichnis = folder
            }
        }

        if (kontakteVerzeichnis != null) {
            val view = ItemView(100)
            val findResults = exchangeService.findItems(kontakteVerzeichnis.id, view)

            //MOOOOOOST IMPORTANT: load messages' properties before
            exchangeService.loadPropertiesForItems(findResults, PropertySet.FirstClassProperties)
            for (item in findResults.getItems()) {
                if (item is Contact) {
                    val contact = item
                    response.add(ContactData(contact.id.toString(), contact.displayName, contact.emailAddresses.getEmailAddress(EmailAddressKey.EmailAddress1).address))
                    println("NAME===" + contact.displayName)
                    println(" PHONENO===" + contact.emailAddresses.getEmailAddress(EmailAddressKey.EmailAddress1))

                }
            }
        }
        return response
    }

    @GetMapping("/events")
    fun findAppointments(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") startDate: Date, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") endDate: Date): List<CalendarEvent> {
        val response = mutableListOf<CalendarEvent>()
        try {
            val cf = CalendarFolder.bind(exchangeService, WellKnownFolderName.Calendar)
            val findResults = cf.findAppointments(CalendarView(startDate, endDate))

            for (appt in findResults.items) {
                appt.load(PropertySet.FirstClassProperties)
                //println("SUBJECT=====" + appt.subject)
                //println("BODY========" + appt.body)
                val attendeeList = mutableListOf<MeetingAttendee>()
                appt.requiredAttendees.forEach(Consumer { ra: Attendee ->
                    attendeeList.add(MeetingAttendee(true, ra.address, ra.name))
                })

                appt.optionalAttendees.forEach(Consumer { ra: Attendee ->
                    attendeeList.add(MeetingAttendee(false, ra.address, ra.name))
                })

                response.add(CalendarEvent(
                        id = appt.id.toString(),
                        subject = appt.subject,
                        body = appt.body.toString(),
                        bodyType = appt.body.bodyType.name,
                        startDate = appt.start,
                        endDate = appt.end,
                        meetingAttendees = attendeeList
                ))
            }
        } catch (exc: Exception) {
            exc.printStackTrace()
        }
        return response
    }

    @PostMapping("/events")
    fun createCalendarEvent(@RequestBody newEvent: NewCalendarEvent): String {
        try {
            val appointment = Appointment(exchangeService)
            appointment.subject = newEvent.subject
            appointment.body = MessageBody.getMessageBodyFromText(newEvent.body)
            appointment.body.bodyType = if (newEvent.bodyType.toLowerCase() == "html") {
                BodyType.HTML
            } else {
                BodyType.Text
            }
            newEvent.meetingAttendees.forEach(Consumer { ma: MeetingAttendee ->
                if (ma.isRequired) {
                    appointment.requiredAttendees.add(Attendee(ma.name, ma.address))
                } else {
                    appointment.optionalAttendees.add(Attendee(ma.name, ma.address))
                }
            })
            appointment.start = newEvent.startDate
            appointment.startTimeZone = exchangeService.serverTimeZones.first()
            appointment.end = newEvent.endDate

            appointment.save()
        } catch (exc: Exception) {
            exc.printStackTrace()
            return "error"
        }
        return ""
    }

    @GetMapping("/myself")
    fun myself(principal: Principal): String {//ContactData
        val ncCol = exchangeService.resolveName(principal.name, ResolveNameSearchLocation.DirectoryOnly, true)
        var mailBox = principal.name
        ncCol.forEach(Consumer { nr: NameResolution ->
            mailBox = nr.mailbox.toString()
        })
        return mailBox
    }
}