package de.hackathon.appointmentapi.msexchangemw.entity

data class ContactData(val id: String, val name: String, val email: String)