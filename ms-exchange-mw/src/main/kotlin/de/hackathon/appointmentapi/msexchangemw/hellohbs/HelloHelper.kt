package de.hackathon.appointmentapi.msexchangemw.hellohbs

import com.google.common.base.Objects
import pl.allegro.tech.boot.autoconfigure.handlebars.HandlebarsHelper

@HandlebarsHelper
class HelloHelper {

    fun sayHello(name: String): String {
        return String.format("Hello %s!", Objects.firstNonNull(name, "unknown"))
    }
}