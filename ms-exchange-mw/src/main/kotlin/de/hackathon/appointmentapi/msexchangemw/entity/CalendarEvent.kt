package de.hackathon.appointmentapi.msexchangemw.entity

import java.util.*

data class CalendarEvent(val id: String,
                         val subject: String,
                         val body: String,
                         val bodyType: String,
                         val startDate: Date,
                         val endDate: Date,
                         val meetingAttendees: List<MeetingAttendee>)

data class NewCalendarEvent(val subject: String,
                            val body: String,
                            val bodyType: String,
                            val startDate: Date,
                            val endDate: Date,
                            val meetingAttendees: List<MeetingAttendee>)

/*
"Subject": "Discuss the Calendar REST API",
  "Body": {
    "ContentType": "HTML",
    "Content": "I think it will meet our requirements!"
  },
  "Start": {
      "DateTime": "2014-02-02T18:00:00",
      "TimeZone": "Pacific Standard Time"
  },
  "End": {
      "DateTime": "2014-02-02T19:00:00",
      "TimeZone": "Pacific Standard Time"
  },
  "Attendees": [
    {
      "EmailAddress": {
        "Address": "janets@a830edad9050849NDA1.onmicrosoft.com",
        "Name": "Janet Schorr"
      },
      "Type": "Required"
    }
  ]
*/