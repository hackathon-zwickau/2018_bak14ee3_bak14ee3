package de.hackathon.appointmentapi.msexchangemw

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MsExchangeMwApplication

fun main(args: Array<String>) {
    runApplication<MsExchangeMwApplication>(*args)
}
